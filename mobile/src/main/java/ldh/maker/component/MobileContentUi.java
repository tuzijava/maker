package ldh.maker.component;

import javafx.scene.control.Tab;

/**
 * Created by ldh on 2017/4/6.
 */
public class MobileContentUi extends TableUi {

    public MobileContentUi() {
    }

    @Override
    protected Tab createContentTab(String selectTable) {
        Tab tab = new Tab();
        tab.setText(selectTable);
        String db = treeItem.getValue().getData().toString();
        CodeUi codeUi = new MobileCodeUi(treeItem, db, selectTable);
        tab.setContent(codeUi);
        return tab;
    }
}
