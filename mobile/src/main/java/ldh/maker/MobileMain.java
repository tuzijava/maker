package ldh.maker;

import ldh.maker.component.MobileContentUiFactory;
import ldh.maker.util.UiUtil;

/**
 * Created by ldh on 2017/4/24.
 */
public class MobileMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new MobileContentUiFactory());
    }

    public static void main(String[] args) throws Exception {
        startDb(args);
        launch(args);
    }
}
