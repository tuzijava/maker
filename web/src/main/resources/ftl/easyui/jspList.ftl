<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="ctx" value="${'${'}pageContext.request.contextPath${'}'}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
	<link rel="stylesheet" type="text/css" href="${'${'}ctx ${'}'}/resource/frame/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="${'${'}ctx${'}'}/resource/frame/easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${'${'}ctx${'}'}/resource/common/css/common.css">
	<script type="text/javascript" src="${'${'}ctx${'}'}/resource/common/js/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="${'${'}ctx${'}'}/resource/frame/easyui/jquery.easyui.min.js"></script>
	<#if util.hasDate(table)>
	<script type="text/javascript" src="${'${'}ctx${'}'}/resource/frame/easyui/easyui-lang-zh_CN.js"></script>
	</#if>
	<script type="text/javascript" src="${'${'}ctx${'}'}/resource/common/js/main.js"></script>
	<script type="text/javascript" src="${'${'}ctx${'}'}/resource/common/js/common.js"></script>
	<script type="text/javascript" src="${'${'}ctx${'}'}/resource/common/js/easyui.js"></script>
	<script type="text/javascript" src="${'${'}ctx${'}'}/resource/js/${util.firstLower(table.javaName)}.js"></script>
</head>
<body>
	<!-- 查询模块 -->
	<div id="p" class="easyui-panel" title="查询" collapsible:true>
    	<form id="searchForm">
		<table>
			<#list util.rows(columns) as row>
			<tr>
				<#list 0..3 as t>
				<#if util.column(columns, (row_index * 4) + t)??>
				<#if util.isDate(util.column(columns, (row_index * 4) + t))>
				<td>${util.comment(util.column(columns, (row_index * 4) + t))}</td><td><input type="text" name="${util.column(columns, (row_index * 4) + t).property}" class="easyui-datetimebox" id="${util.column(columns, (row_index * 4) + t).property}"/></td>
				<#elseif util.isEnum(util.column(columns, (row_index * 4) + t))>
				<select name="${util.column(columns, (row_index * 4) + t).property}" class="easyui-combobox">
					<option value ="">请选择</option>
					<c:forEach var="e" items="${r"${"}${util.column(columns, (row_index * 4) + t).property}List${r"}"}">
						<option value ="${r"${"}e${r"}"}">${r"${"}e.desc${r"}"}</option>
					</c:forEach>
				</select>
				<#else>
				<#if util.column(columns, (row_index * 4) + t).foreign>
				<td>${util.comment(util.column(columns, (row_index * 4) + t))}</td><td><input type="text"  id="${util.column(columns, (row_index * 4) + t).property}IdSearch"/>
					<input type="hidden" id="${util.column(columns, (row_index * 4) + t).property}IdSearchHidden" name="${util.column(columns, (row_index * 4) + t).property}.${util.column(columns, (row_index * 4) + t).foreignKey.foreignColumn.property}"/>
					<input type="button" onclick="select${util.firstUpper(util.column(columns, (row_index * 4) + t).property)}('Search')" value="选择"/>
				</td>
				<#else>
				<td>${util.comment(util.column(columns, (row_index * 4) + t))}</td><td><input type="text" name="${util.column(columns, (row_index * 4) + t).property}"/></td>
				</#if>
				</#if>
				</#if>
				</#list>
				<#if row_index == 0>
				<td rowspan="${util.rows(columns)?size}"><input type="button" value="查询"  onclick="search()"/> <input type="reset" value="重置"/></td>
				</#if>
			</tr>
			</#list>
		</table>
		</form>
	</div>
	<br/>
	
	<!-- 查询模块中的工具栏 -->
	<div id="tb" style="height:auto">
	    <a href="javascript:info_add()" class="easyui-linkbutton" iconCls="icon-add" plain="true"></a>
		<a href="javascript:info_edit()" class="easyui-linkbutton" iconCls="icon-edit" plain="true"></a>
		<a href="javascript:info_remove()" class="easyui-linkbutton" iconCls="icon-remove" plain="true"></a>
	</div>
	
	<!-- 数据列表           -->
	<table id="table" class="easyui-datagrid" title="列表" pagination="true"
		   rownumbers="true" fitColumns="true" singleSelect="true" toolbar="#tb">
		<thead>
			<tr>
				<#list table.columnList as column>
				<#if column.create>
				<#if column.foreign>
				<th field="${column.property}" formatter="lowerName('${column.property}', '${column.foreignKey.foreignColumn.property}')">${util.comment(column)}</th>
				<#list column.foreignKey.foreignTable.columnList as cc>
				<#if cc.property == "name" || util.contains(cc.property, "Name")>
				<th field="${column.property}2" formatter="lowerName('${column.property}', '${cc.property}')">${util.comment(column)}-${util.comment(cc)}</th>
				</#if>
				</#list>
				<#elseif util.isEnum(column)>
				<th field="${column.property}" formatter="lowerName('${column.property}', 'desc')">${util.comment(column)}</th>
				<#else>
				<th field="${column.property}">${util.comment(column)}</th>
				</#if>
				</#if>
				</#list>
			</tr>
		</thead>
	</table>
	
	<!-- 添加信息或修改信息       -->
	<div id="info" class="easyui-dialog" title="详细信息" style="width:450px;height:${table.columnList?size * 41}px; padding:10px 20px"
        resizable="true" modal="true" closed="true" buttons="#dlg-buttons">
        <div class="form_title">${table.javaName} Information</div>
        <form id="${util.firstLower(table.javaName)}Info" class="form">
        <#if table.primaryKey.column??>
    	 	<input type="hidden" name="${table.primaryKey.column.property}" id="${table.primaryKey.column.property}"/>
    	 </#if>
        <#list table.columnList as column>
        	<#if table.primaryKey.column?? && table.primaryKey.column.property != column.property>
			<#if column.foreign>
			<div class="form_item">
				<label>${util.comment(column)}:</label>
				<input type="text" id="${column.property}${util.firstUpper(column.foreignKey.foreignColumn.property)}Add" class="easyui-textbox"/>
				<input type="hidden" name="${column.property}.${column.foreignKey.foreignColumn.property}" id="${column.property}${util.firstUpper(column.foreignKey.foreignColumn.property)}AddHidden" value=""/>
				<input type="button" onclick="select${util.firstUpper(column.property)}('Add')" value="选择" style="width:40px"/>
			</div>
        	<#elseif util.isDate(column)>
        	<div class="form_item">
				<label>${util.comment(column)}:</label>
				<input type="text" name="${column.property}" class="easyui-datetimebox"/>
			</div>
        	<#elseif util.isNumber(column)>
        	<div class="form_item">
				<label>${util.comment(column)}:</label>
				<input type="text" name="${column.property}" <#if util.easyuiValiType(column)??>class="easyui-numberbox"  ${util.easyuiValiType(column)} </#if>/>
			</div>
			<#elseif util.isEnum(column)>
			<div class="form_item">
				<label>${util.comment(column)}:</label>
				<select name="${column.property}" id="${column.property}" class="easyui-combobox">
					<option value ="">请选择</option>
					<c:forEach var="e" items="${r"${"}${column.property}List${r"}"}">
						<option value ="${r"${"}e${r"}"}">${r"${"}e.desc${r"}"}</option>
					</c:forEach>
				</select>
			</div>
        	<#else>
        	<div class="form_item">
				<label>${util.comment(column)}:</label>
				<input type="text" name="${column.property}" <#if util.easyuiValiType(column)??>class="easyui-validatebox" ${util.easyuiValiType(column)} </#if>/>
			</div>
        	</#if>
			</#if>
		</#list>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="info_click()" style="width:90px">提交</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#info').dialog('close')" style="width:90px">Cancel</a>
	</div>
	
	<#list table.columnList as column>
	<#if column.foreign>
	<!-- 获取 ${column.property}的弹出框     -->
	<div id="${column.property}Dialog" class="easyui-dialog" title="选择${util.comment(column)}" style="width:450px;height:450px;"
	        resizable="true" modal="true">
	    <div id="tb${util.firstUpper(column.property)}" style="height:auto">
	    	<a href="javascript:add${util.firstUpper(column.property)}()" class="easyui-linkbutton" iconCls="icon-add" plain="true"></a>
		</div>
		
		<table id="${column.property}Table" class="easyui-datagrid" title="列表" singleSelect="true" pagination="true" toolbar="#tb${util.firstUpper(column.property)}">
			<thead>
				<tr>
					<#if column.foreignKey.foreignTable.primaryKey??>
					<th field="${column.foreignKey.foreignTable.primaryKey.column.property}">${util.comment(column.foreignKey.foreignTable.primaryKey.column)}</th>
					</#if>
					<#list column.foreignKey.foreignTable.columnList as cc>
					<#if cc.property == "name" || util.contains(cc.property, "Name")>
					<th field="${cc.property}">${util.comment(cc)}</th>
					</#if>
					</#list>
				</tr>
			</thead>
		</table>
	</div>
	
	</#if>
	</#list>
	</body>
</html>