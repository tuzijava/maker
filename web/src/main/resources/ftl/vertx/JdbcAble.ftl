package ${beanPackage};

import co.paralleluniverse.fibers.Instrumented;
import co.paralleluniverse.fibers.Suspendable;

/**
 * Created by ldh123 on 2018/6/24.
 */
@FunctionalInterface
public interface JdbcAble<T, R> {

    @Instrumented
    @Suspendable
    R apply(T t);
}
