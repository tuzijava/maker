<%@ tag language="java" pageEncoding="UTF-8"  body-content="scriptless"%>
<%@ attribute name="label" required="true"%>
<%@ attribute name="name" required="true"%>

<div class="form-group">
    <label for="${name}" class="col-sm-2 control-label">${label}</label>
    <div class="col-sm-10">
        <jsp:doBody/>
    </div>
</div>