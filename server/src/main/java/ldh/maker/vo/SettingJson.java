package ldh.maker.vo;

/**
 * Created by ldh123 on 2018/5/26.
 */
public class SettingJson {

    /**
     * 是否使用Lombok生成对象
     */
    private boolean isLombok = true;

    private boolean isShiro = false;

    private boolean isCrossDomain = false;

    private String javafxPojoPath;

    public boolean isLombok() {
        return isLombok;
    }

    public void isLombok(boolean isLombok) {
        this.isLombok = isLombok;
    }

    public void setLombok(boolean lombok) {
        isLombok = lombok;
    }

    public String getJavafxPojoPath() {
        return javafxPojoPath;
    }

    public void setJavafxPojoPath(String javafxPojoPath) {
        this.javafxPojoPath = javafxPojoPath;
    }

    public boolean isShiro() {
        return isShiro;
    }

    public void setShiro(boolean shiro) {
        isShiro = shiro;
    }

    public boolean isCrossDomain() {
        return isCrossDomain;
    }

    public void setCrossDomain(boolean crossDomain) {
        isCrossDomain = crossDomain;
    }
}
